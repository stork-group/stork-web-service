const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const deviceSchema = mongoose.Schema({
  dev_code: { type: String, required: true, unique: true },
  available: { type: String, required: true },
  ownerId: { type: String, required: false }
});

deviceSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Device', deviceSchema);
