const mongoose = require('mongoose');

const pointSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: ['Point'],
    required: true
  },
  coordinates: {
    type: [Number],
    required: true
  }
});

const storkSchema = mongoose.Schema({
  stork_code: { type: String, required: true, unique: true },
  lastSeen: { type: String, default: 'No Communication Has Occured Yet.' },
  nickname: { type: String, required: true },
  ownerId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  location: {
    type: pointSchema,
    required: true
  },
  statusCode: { type: Number, required: true },
  statusMessage: { type: String, default: 'Device Status Unknown.' },
  temperature: { type: Number },
  humidity: { type: Number }
});

module.exports = mongoose.model('Stork', storkSchema);
