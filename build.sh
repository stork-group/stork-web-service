#!/bin/bash
# docker image prune -a
# Delete all containers
docker rm -f $(docker ps -a -q)
# Delete all images
docker rmi -f $(docker images -q)
# Run
docker-compose build 
docker-compose up --force-recreate

