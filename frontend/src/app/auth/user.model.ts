export interface User {
  fname: string;
  sname: string;
  address: string;
  phoneNumber: string;
  username: string;
  email: string;
  password: string;
  plan: string;
}
