export interface Device {
  stork_code: string;
  available: string;
  ownerId: string;
}
