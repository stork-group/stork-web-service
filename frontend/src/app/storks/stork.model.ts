export interface Stork {
  id: string;
  stork_code: string;
  lastSeen: string;
  nickname: string;
  gpsType: string;
  latitude: number;
  longitude: number;
  statusCode: number;
  statusMessage: string;
  temperature: number;
  humidity: number;
}
