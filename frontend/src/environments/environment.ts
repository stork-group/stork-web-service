export const environment = {
  production: false,
  baseUrl: 'http://localhost',
  apiUrl: 'http://localhost' + ':3000/api'
};
